﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Автосервис_Колесо_фартуны
{
    public partial class UsersForm : Form
    {
        public UsersForm()
        {
            InitializeComponent();
        }

        private void UsersForm_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection connection = new SqlConnection(ServerInfo.connectionString);
                SqlCommand command = new SqlCommand("Select * from Service",connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                List<string[]> list = new List<string[]>();


                if (reader.HasRows)
                {
                
                    while (reader.Read())
                    {
                        list.Add(new string[5]);
                        list[list.Count - 1][0] = reader[1].ToString();
                        list[list.Count - 1][1] = reader[2].ToString();
                        list[list.Count - 1][2] = reader[3].ToString();
                        list[list.Count - 1][3] = reader[4].ToString();
                        list[list.Count - 1][4]=reader[5].ToString();

                    }
                    foreach (string[] s in list)
                    {
                        ListViewItem item = new ListViewItem(s);
                        listView1.Items.Add(item);
                    }
                }

                connection.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }

           
        }
    }
}
