﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Автосервис_Колесо_фартуны
{
    public partial class AdministrationForm : Form
    {
        int redact = 0;
        public AdministrationForm()
        {
            InitializeComponent();
        }

        private void AdministrationForm_Load(object sender, EventArgs e)
        {
            LoadList();
        }
        private void LoadList()
        {
            SqlConnection connection = new SqlConnection(ServerInfo.connectionString);
            SqlCommand command = new SqlCommand("Select * from Service", connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                int i = 0;
                while (reader.Read())
                {
                    TableLayoutPanel panel = new TableLayoutPanel();
                    panel.ColumnCount = 1;
                    panel.RowCount = 3;
                    panel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                    panel.Dock = DockStyle.Fill;
                    panel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    Label label_one = new Label();
                    label_one.AutoSize = true;
                    label_one.MaximumSize = new Size(150, 100);
                    label_one.Text = reader[1].ToString();
                    Label label_two = new Label();
                    label_two.Text = reader[2].ToString();
                    Label label_third = new Label();

                    PictureBox picture = new PictureBox();
                    picture.Name = "pic_" + reader[0].ToString();
                    picture.MouseClick += pictureClick;
                    picture.Image = Properties.Resources.Снимок;
                    picture.SizeMode = PictureBoxSizeMode.StretchImage;
                    picture.Anchor = AnchorStyles.Left;
                    picture.Height = 100;
                    label_third.Text = reader[5].ToString();
                    tableLayoutPanel1.Controls.Add(panel, 0, i);
                    tableLayoutPanel1.Controls.Add(picture, 1, i);
                    tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
                    i++;
                    panel.Controls.Add(label_one, 0, 0);
                    panel.Controls.Add(label_two, 0, 1);
                    panel.Controls.Add(label_third, 0, 2);

                }
                tableLayoutPanel1.RowStyles[0].Height = 50F;
            }
        }
        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
         
            
                Graphics graphics = e.Graphics;
                Rectangle r = e.CellBounds;
                graphics.FillRectangle(Brushes.Gray, r);
            
        }
        private void pictureClick(object sender, MouseEventArgs e)
        {
            var pic = (sender as PictureBox);
            string id = pic.Name.Remove(0,4);
            redact = Convert.ToInt32(id);
            SqlConnection connection = new SqlConnection(ServerInfo.connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand($"Select * from Service where ID={id}",connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                button1.Enabled = true;
                while (reader.Read())
                {
                    textBox1.Text = reader[1].ToString();
                    textBox2.Text = reader[4].ToString();
                    textBox5.Text = reader[2].ToString();
                    textBox4.Text = (Convert.ToDouble(reader[3].ToString())/60).ToString();
                    textBox3.Text = (Convert.ToDouble(reader[5])*100).ToString();
                }
            }
           
            connection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                if (Convert.ToInt32(textBox3.Text) > 100 || Convert.ToInt32(textBox3.Text) < 0)
                {
                    MessageBox.Show("Такой скидки не существует, пожалуйста укажите значение от 0 до 100");
                }
                else
                {
                    double d = Convert.ToDouble(textBox4.Text) * 60;
                    double disc = Convert.ToDouble(textBox3.Text);
                    string fl = "";
                    if (disc != 0)
                    {
                        disc = disc / 100;
                        
                    }
                    fl = disc.ToString().Replace(',', '.');
                    SqlConnection connection = new SqlConnection(ServerInfo.connectionString);
                    connection.Open();
                    SqlCommand command = new SqlCommand($"update Service set Title='{textBox1.Text}',Cost='{Convert.ToDouble(textBox5.Text)}',DurationInSeconds='{d}',[Description]='{textBox2.Text}',Discount='{fl}' where ID={redact}", connection);
                    command.ExecuteNonQuery();
                    connection.Close();

                }
            }
            else
            {
                MessageBox.Show("Все поля должны быть заполнены");
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
