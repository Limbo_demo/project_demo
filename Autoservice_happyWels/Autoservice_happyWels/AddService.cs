﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Autoservice_happyWels
{
    public partial class AddService : Form
    {
        public AddService()
        {
            InitializeComponent();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                double Cost = 0;
                double disc = 0;
                int min = 0;
                try
                {

                    Cost = Convert.ToDouble(textBox3.Text);
                    min = Convert.ToInt32(textBox5.Text);
                    if (min > 0 && min<=240)
                        min = min * 60;
                    else
                    {
                        MessageBox.Show("Длительность услуги не может быть ровна 0 или превышать 240 минут");
                        return;
                    }

                    if (Convert.ToInt32(textBox4.Text) > 0 && Convert.ToInt32(textBox4.Text) < 100)
                    {
                        disc = Convert.ToDouble(textBox4.Text) / 100;

                    }
                    else
                    {
                        MessageBox.Show("Недопустимое значение скидки");
                        return;
                    }
                    SqlConnection connection = new SqlConnection(ServerInfo.Connect);
                    connection.Open();
                    SqlCommand command_isHave = new SqlCommand($"Select * from Service where Title='{textBox1.Text}'",connection);
                    SqlDataReader reader = command_isHave.ExecuteReader();
                    if (reader.HasRows)
                    {
                        MessageBox.Show("Услуга с таким названием уже существует в базе данных");
                        reader.Close();
                    }
                    else
                    {
                        SqlCommand command = new SqlCommand($"Insert into [Service] (Title,Cost,[Description],DurationInSeconds,Discount)values('{textBox1.Text}','{Cost}','{textBox2.Text}','{min}','{disc.ToString().Replace(',', '.')}')", connection);
                        command.ExecuteNonQuery();
                        connection.Close();
                        textBox1.Text = "";
                        textBox2.Text = "";
                        textBox3.Text = "";
                        textBox4.Text = "";
                        textBox5.Text = "";
                        DialogResult = DialogResult.OK;
                        Hide();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + " " + ex.Data);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля");
            }
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if ((!Char.IsDigit(e.KeyChar)) && number != 8 && number != 44)
            {
                if (e.KeyChar == ',')
                {
                    if (textBox3.Text.IndexOf(',') != -1)
                    {
                        e.Handled = true;
                    }
                    else
                    {
                        return;
                    }

                }
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(e.KeyChar) && number != 8)
            {
               
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(e.KeyChar) && number != 8)
            {
               
                e.Handled = true;
            }
        }

        private void AddService_Load(object sender, EventArgs e)
        {

        }
    }
}
