﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Autoservice_happyWels
{
    public partial class Form1 : Form
    {
   int countClick = 0;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadList();

        }
        private void LoadList()
        {
            SqlConnection connection = new SqlConnection(ServerInfo.Connect);
            SqlCommand command = new SqlCommand("Select * from Service",connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
     
                int i = 0;
                try
                {
                    while (reader.Read())
                    {
                        TableLayoutPanel panel = new TableLayoutPanel();
                        panel.Anchor = AnchorStyles.Left;
                        if (reader[5].ToString() == "0")
                        {
                            panel.RowCount = 3;
                        }
                        else
                        {
                            panel.BackColor = Color.LimeGreen;
                            panel.RowCount = 4;
                        }
                        panel.ColumnCount = 1;
                        panel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                        panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));
                        
    
                        PictureBox pic = new PictureBox();
                        pic.Name = "pic_" + reader[0].ToString();
                        pic.SizeMode = PictureBoxSizeMode.StretchImage;
                        pic.Anchor = AnchorStyles.Right;
                        pic.Image = Properties.Resources.button;
                        pic.Height = 100;
                        pic.Dock = DockStyle.Left;
                        pic.MouseClick += Click_pic;
                        pic.Cursor=Cursors.Hand ;
                        Label label_one = new Label();
                        label_one.AutoSize = true;
                        label_one.Width = 50;
                        
                        label_one.Height = 100;
                        label_one.Anchor = AnchorStyles.None;
                        label_one.Text = reader[1].ToString();
                        Label label_Two = new Label();

                        Label label_third = new Label();
                        double procent = Convert.ToDouble(reader[5].ToString()) * 100;
                        label_third.Text = "Скидка: " + Convert.ToInt32(procent).ToString() + "%";
                        label_third.Anchor = AnchorStyles.None;
                        label_third.AutoSize = true;

                        double cost = (Convert.ToDouble(reader[2].ToString()) / 100) * Convert.ToInt32(procent);
                        double cost_do = Convert.ToDouble(reader[2].ToString()) - cost;

                        label_Two.Text ="Стоимость: "+Convert.ToDouble(cost_do).ToString()+" р";
                        if (reader[5].ToString() == "0")
                        {
                            label_Two.ForeColor = Color.Orange;
                        }
                        else
                        {
                            label_Two.ForeColor = Color.Yellow;
                        }
                        label_Two.Anchor = AnchorStyles.None;
                        label_Two.AutoSize = true;
                        if (reader[5].ToString() != "0")
                        {
                            Label Lable_five = new Label();
                            Lable_five.Text = Convert.ToDouble(reader[2].ToString()).ToString() + "р";
                            Lable_five.Anchor = AnchorStyles.None;
                            Lable_five.AutoSize = true;
                            Lable_five.Font = new Font(Lable_five.Font.FontFamily, Lable_five.Font.Size, FontStyle.Strikeout);
                            panel.Controls.Add(Lable_five, 0, 3);
                        }

                        panel.Controls.Add(label_one,0, 0);
                        panel.Controls.Add(label_Two, 0, 1);
                        panel.Controls.Add(label_third, 0, 2);

                        
                       

                        tableLayoutPanel1.Controls.Add(panel, 0, i);
                        tableLayoutPanel1.Controls.Add(pic, 1, i);
                        tableLayoutPanel1.RowCount++;
                        tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent,50f));
                     
                        i++;

                    }

         
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message+ " "+ex.StackTrace);
                }
                
             
            }
            connection.Close();
        }
        private void Filter()
        {

            if (comboBox1.SelectedIndex != -1)
            {
                while (tableLayoutPanel1.RowCount > 0)
                {
                    if (tableLayoutPanel1.RowCount != 0)
                    {
                        tableLayoutPanel1.Controls[0].Dispose();
                        break;
                    }
                }
                tableLayoutPanel1.Controls.Clear();
                SqlConnection connection = new SqlConnection(ServerInfo.Connect);
                string request = "Select * from Service ";
                if (comboBox1.SelectedIndex != -1)
                {
                    if(comboBox1.SelectedIndex==0)
                    request += " ORDER BY Cost asc";
                    else
                        request += " ORDER BY Cost desc";
                    request += " where";
                }
                if (request.Substring(request.Length - 5) == "where")
                {
                  request=  request.Remove(request.Length - 5);
                }
                SqlCommand command = new SqlCommand(request, connection);
                connection.Open();
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {

                    int i = 0;
                    try
                    {
                        while (reader.Read())
                        {
                            TableLayoutPanel panel = new TableLayoutPanel();
                            panel.Anchor = AnchorStyles.Left;
                            if (reader[5].ToString() == "0")
                            {
                                panel.RowCount = 3;
                            }
                            else
                            {
                                panel.BackColor = Color.LimeGreen;
                                panel.RowCount = 4;
                            }
                            panel.ColumnCount = 1;
                            panel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                            panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));


                            PictureBox pic = new PictureBox();
                            pic.Name = "pic_" + reader[0].ToString();
                            pic.SizeMode = PictureBoxSizeMode.StretchImage;
                            pic.Anchor = AnchorStyles.Right;
                            pic.Image = Properties.Resources.button;
                            pic.Height = 100;
                            pic.Dock = DockStyle.Left;
                            pic.MouseClick += Click_pic;
                            pic.Cursor = Cursors.Hand;
                            Label label_one = new Label();
                            label_one.AutoSize = true;
                            label_one.Width = 50;

                            label_one.Height = 100;
                            label_one.Anchor = AnchorStyles.None;
                            label_one.Text = reader[1].ToString();
                            Label label_Two = new Label();

                            Label label_third = new Label();
                            double procent = Convert.ToDouble(reader[5].ToString()) * 100;
                            label_third.Text = "Скидка: " + Convert.ToInt32(procent).ToString() + "%";
                            label_third.Anchor = AnchorStyles.None;
                            label_third.AutoSize = true;

                            double cost = (Convert.ToDouble(reader[2].ToString()) / 100) * Convert.ToInt32(procent);
                            double cost_do = Convert.ToDouble(reader[2].ToString()) - cost;

                            label_Two.Text = "Стоимость: " + Convert.ToDouble(cost_do).ToString() + " р";
                            if (reader[5].ToString() == "0")
                            {
                                label_Two.ForeColor = Color.Orange;
                            }
                            else
                            {
                                label_Two.ForeColor = Color.Yellow;
                            }
                            label_Two.Anchor = AnchorStyles.None;
                            label_Two.AutoSize = true;
                            if (reader[5].ToString() != "0")
                            {
                                Label Lable_five = new Label();
                                Lable_five.Text = Convert.ToDouble(reader[2].ToString()).ToString() + "р";
                                Lable_five.Anchor = AnchorStyles.None;
                                Lable_five.AutoSize = true;
                                Lable_five.Font = new Font(Lable_five.Font.FontFamily, Lable_five.Font.Size, FontStyle.Strikeout);
                                panel.Controls.Add(Lable_five, 0, 3);
                            }

                            panel.Controls.Add(label_one, 0, 0);
                            panel.Controls.Add(label_Two, 0, 1);
                            panel.Controls.Add(label_third, 0, 2);

                            tableLayoutPanel1.Controls.Add(panel, 0, i);
                            tableLayoutPanel1.Controls.Add(pic, 1, i);
                            tableLayoutPanel1.RowCount++;
                            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));

                            i++;

                        }


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + " " + ex.StackTrace);
                    }


                }
                else
                {
                    MessageBox.Show("Ничего не найдено");
                }
                connection.Close();
            }
            else
            {
                
                LoadList();
                while (tableLayoutPanel1.RowCount > 0)
                {
                    if (tableLayoutPanel1.RowCount != 0)
                    {
                        tableLayoutPanel1.Controls[0].Dispose();
                        break;
                    }
                }
                tableLayoutPanel1.Controls.Clear();
            }
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Rectangle r = e.CellBounds;
            graphics.FillRectangle(Brushes.Gray, r);
        }
        private void Click_pic(object sender,MouseEventArgs e)
        {
            var picture = (sender as PictureBox);
            string Id = picture.Name.Remove(0, 4);
            SqlConnection connection = new SqlConnection(ServerInfo.Connect);
            connection.Open();
            SqlCommand command = new SqlCommand($"Select * from Service where ID={Id}",connection);
            SqlDataReader reader=command.ExecuteReader();
            if (reader.HasRows)
            {
                groupBox1.Visible = true;
                while (reader.Read())
                {
                    groupBox1.Text = "Услуга:" + reader[1].ToString();
                    label2.Text = reader[4].ToString();
                    double min = Convert.ToDouble(reader[3].ToString()) /60;
                    string disc = reader[5].ToString();

                    double discount = Convert.ToDouble(disc.ToString()) * 100;

                    label3.Text = "Длительность:"+Convert.ToDouble(min)+" мин";
                    label1.Text = "Стоимость:" + Convert.ToDouble(reader[2].ToString()) + " р";
                    label4.Text = "Скидка:" + discount + "%";

                }
            }


            connection.Close();
        }
        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '0')
            {
                countClick++;
                if (countClick == 4)
                {
                    AdminForm form = new AdminForm();
                    form.Show();
                    Hide();
                }
            }
            else
            {
                countClick = 0;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '0')
            {
                countClick++;
                if (countClick == 4)
                {
                    AdminForm form = new AdminForm();
                    form.Show();
                    Hide();
                }
            }
            else
            {
                countClick = 0;
            }
        }

        private void button1_Leave(object sender, EventArgs e)
        {
            countClick = 0;
        }
    }
}
