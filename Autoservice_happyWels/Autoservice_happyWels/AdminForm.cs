﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Autoservice_happyWels
{
    public partial class AdminForm : Form
    {
        int RedactId = 0;
        public AdminForm()
        {
            InitializeComponent();
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            LoadList();
        }
        private void LoadList()
        {
            SqlConnection connection = new SqlConnection(ServerInfo.Connect);
            SqlCommand command = new SqlCommand("Select * from Service", connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {

                int i = 0;
                try
                {
                    while (reader.Read())
                    {
                        TableLayoutPanel panel = new TableLayoutPanel();
                        panel.Anchor = AnchorStyles.Left;
                        if (reader[5].ToString() == "0")
                        {
                            panel.RowCount = 3;
                        }
                        else
                        {
                            panel.BackColor = Color.LimeGreen;
                            panel.RowCount = 4;
                        }
                        panel.ColumnCount = 1;
                        panel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                        panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50));


                        PictureBox pic = new PictureBox();
                        pic.Name = "pic_" + reader[0].ToString();
                        pic.SizeMode = PictureBoxSizeMode.StretchImage;
                        pic.Anchor = AnchorStyles.Right;
                        pic.Image = Properties.Resources.button;
                        pic.Height = 100;
                        pic.Dock = DockStyle.Left;
                        pic.MouseClick += Click_pic;
                        pic.Cursor = Cursors.Hand;
                        Label label_one = new Label();
                        label_one.AutoSize = true;
                        label_one.Width = 50;

                        label_one.Height = 100;
                        label_one.Anchor = AnchorStyles.None;
                        label_one.Text = reader[1].ToString();
                        Label label_Two = new Label();

                        Label label_third = new Label();
                        double procent = Convert.ToDouble(reader[5].ToString()) * 100;
                        label_third.Text = "Скидка: " + Convert.ToInt32(procent).ToString() + "%";
                        label_third.Anchor = AnchorStyles.None;
                        label_third.AutoSize = true;

                        double cost = (Convert.ToDouble(reader[2].ToString()) / 100) * Convert.ToInt32(procent);
                        double cost_do = Convert.ToDouble(reader[2].ToString()) - cost;

                        label_Two.Text = "Стоимость: " + Convert.ToDouble(cost_do).ToString() + " р";
                        if (reader[5].ToString() == "0")
                        {
                            label_Two.ForeColor = Color.Orange;
                        }
                        else
                        {
                            label_Two.ForeColor = Color.Yellow;
                        }
                        label_Two.Anchor = AnchorStyles.None;
                        label_Two.AutoSize = true;
                        if (reader[5].ToString() != "0")
                        {
                            Label Lable_five = new Label();
                            Lable_five.Text = Convert.ToDouble(reader[2].ToString()).ToString() + "р";
                            Lable_five.Anchor = AnchorStyles.None;
                            Lable_five.AutoSize = true;
                            Lable_five.Font = new Font(Lable_five.Font.FontFamily, Lable_five.Font.Size, FontStyle.Strikeout);
                            panel.Controls.Add(Lable_five, 0, 3);
                        }

                        panel.Controls.Add(label_one, 0, 0);
                        panel.Controls.Add(label_Two, 0, 1);
                        panel.Controls.Add(label_third, 0, 2);




                        tableLayoutPanel1.Controls.Add(panel, 0, i);
                        tableLayoutPanel1.Controls.Add(pic, 1, i);
                        tableLayoutPanel1.RowCount++;
                        tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));

                        i++;

                    }


                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + " " + ex.StackTrace);
                }


            }
            connection.Close();
        }
        private void Click_pic(object sender, MouseEventArgs e)
        {
            var picture = (sender as PictureBox);
            string Id = picture.Name.Remove(0, 4);
            SqlConnection connection = new SqlConnection(ServerInfo.Connect);
            connection.Open();
            SqlCommand command = new SqlCommand($"Select * from Service where ID={Id}", connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                RedactId = Convert.ToInt32(Id);
                groupBox1.Visible = true;
                while (reader.Read())
                {
                    groupBox1.Text = "Услуга:";
                    textBox2.Text = reader[1].ToString();
                    textBox1.Text = reader[4].ToString();
                    double min = Convert.ToDouble(reader[3].ToString()) / 60;
                    string disc = reader[5].ToString();

                    double discount = Convert.ToDouble(disc.ToString()) * 100;

                    textBox3.Text = Convert.ToDouble(min).ToString() ;
                    textBox4.Text = Convert.ToDouble(reader[2].ToString()).ToString();
                    textBox5.Text = discount.ToString();
                    
                }
            }


            connection.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                double min = Convert.ToDouble(textBox3.Text) * 60;
                double cost = Convert.ToDouble(textBox4.Text);
                double disc = Convert.ToDouble(textBox5.Text);
                if (disc > 100 || disc < 0)
                {
                    MessageBox.Show("Некорректное значение скидки услуги");
                    return;
                }
                if (disc != 0)
                {
                    disc= disc/ 100;
                }
                SqlConnection connection = new SqlConnection(ServerInfo.Connect);
                connection.Open();

                SqlCommand command = new SqlCommand($"update Service set Title='{textBox2.Text}',Cost='{cost}',DurationInSeconds='{min}',[Description]='{textBox1.Text}',Discount='{Convert.ToString(disc).Replace(',','.')}' where ID={RedactId}", connection);
               
                command.ExecuteNonQuery();
                while (tableLayoutPanel1.RowCount > 0)
                {
                    if (tableLayoutPanel1.RowCount != 0)
                    {
                        tableLayoutPanel1.Controls[0].Dispose();
                        break;
                    }
                }
                tableLayoutPanel1.Controls.Clear();
                LoadList();
                connection.Close();
                groupBox1.Visible = false;
            }
            else
            {
                MessageBox.Show("Все поля должны быть заполнены!");
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Rectangle r = e.CellBounds;
            graphics.FillRectangle(Brushes.Gray, r);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            form.Show();
            Hide();
        }

        private void добавитьУслугуToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddService from = new AddService();
            from.ShowDialog();
            if (from.DialogResult == DialogResult.OK)
            {
                while (tableLayoutPanel1.RowCount > 0)
                {
                    if (tableLayoutPanel1.RowCount != 0)
                    {
                        tableLayoutPanel1.Controls[0].Dispose();
                        break;
                    }
                }
                tableLayoutPanel1.Controls.Clear();
                LoadList();
                groupBox1.Visible = false;
            }
            
        }
    }
}
